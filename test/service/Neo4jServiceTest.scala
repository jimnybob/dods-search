package service

import org.junit.{Assert, Test}

/**
 * Created by James on 04/04/2014.
 */
class Neo4jServiceTest {

  @Test
  def find() = {

    val rows = Neo4jService.findData("Food/Fruit")
    println(rows)
    Assert.assertEquals(2, rows.size)
  }

  @Test
  def invalidPath() = {

    val rows = Neo4jService.findData("Meat/Fruit")
    Assert.assertEquals(0, rows.size)
  }

  @Test
  def buildLongPath = {

    val expected = "MATCH (cat0 {name: 'Food'})<-[]-(cat1 {name:'Fruit'})<-[]-(cat2 {name:'Apples'})<-[rel]-(subcat) RETURN DISTINCT subcat.name as catName, rel.name as relName LIMIT 25"
    Assert.assertEquals(expected, Neo4jService.buildQuery("Food/Fruit/Apples"))
  }

  @Test
  def buildShortPath = {

    val expected = "MATCH (cat0 {name: 'Food'})<-[rel]-(subcat) RETURN DISTINCT subcat.name as catName, rel.name as relName LIMIT 25"
    Assert.assertEquals(expected, Neo4jService.buildQuery("Food"))
  }

  @Test
  def queryBySupermarket = {
    val expected = "MATCH (cat0 {name: 'Meat'})<-[]-(cat1 {name:'Beef'})<-[rel]-(subcat)<-[*0..4]-(shop:Store {name: 'Tesco'}) RETURN DISTINCT subcat.name as catName, rel.name as relName LIMIT 25"
    Assert.assertEquals(expected, Neo4jService.buildQuery("Supermarket/Tesco/Meat/Beef"))
  }
}

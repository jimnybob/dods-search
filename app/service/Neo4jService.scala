package service

import org.anormcypher._

/**
 * Created by James on 04/04/2014.
 */
object Neo4jService {

  case class Row(graphPath: String, relation: Option[String])

  def findData(path: String) = {

    Neo4jREST.setServer("localhost", 7474, "/db/data/")

    Cypher(buildQuery(path)).apply().map(row =>
      Row("/" + path + "/" + row[String]("catName"), (row[Option[String]]("relName")))
    ).toList
  }

  /**
   * Inspiration taken from mkString
   * @param path
   * @return
   */
  def buildQuery(path: String) = {

    val b = new StringBuilder()

    var nodes = path.split("/")

    // Hmm logic to make search by supermarket work
    var queryBySupermarket = ""
    if (nodes(0) == "Supermarket" && nodes.length > 1) {
      queryBySupermarket = "<-[*0..4]-(shop:Store {name: '" + nodes(1) + "'})"
      nodes = nodes.drop(2)
      if(nodes.isEmpty) {
        nodes = nodes :+ "Food"
      }
    }


    var first = true

    var i = 1
    b append "MATCH (cat0 {name: '"
    for (x <- nodes) {
      if (first) {
        b append x append "'})"
        first = false
      }
      else {
        b append "<-[]-(cat" + i + " {name:'" + x + "'})"

        i += 1
      }
    }

    b append "<-[rel]-(subcat)"
    b append queryBySupermarket
    b append " RETURN DISTINCT subcat.name as catName, rel.name as relName LIMIT 25"

    b.toString()
  }
}

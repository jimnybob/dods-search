package controllers

import play.api._
import play.api.mvc._
import service.Neo4jService
import service.Neo4jService.Row

object Application extends Controller {

  def start() = Action {
    Ok(views.html.index(List(Row("/Food", None), Row("/Supermarket", None))))
  }

  def query(path: String) = Action {

    val results = Neo4jService.findData(path)
    Ok(views.html.index(results))
  }

}
import play.Project._

name := "dods-search"

version := "1.0"

resolvers ++= Seq(
  "anormcypher" at "http://repo.anormcypher.org/",
  "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/",
  "mvn-neo4j" at "http://m2.neo4j.org/content/groups/everything"
)

libraryDependencies += "org.anormcypher" %% "anormcypher" % "0.4.4"

libraryDependencies += "org.neo4j" % "neo4j" % "2.1.0-M01" classifier "docs"

libraryDependencies += "org.neo4j" % "neo4j-rest-graphdb" % "2.0.1"

libraryDependencies += "com.sun.jersey" % "jersey-core" % "1.9"

playScalaSettings